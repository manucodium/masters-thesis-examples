import java.lang.ref.WeakReference;

class InnerClassTest {
    int x;
    int y;

    static class StaticInnerClass {
        WeakReference<InnerClassTest> thisInstance;

        StaticInnerClass(InnerClassTest thisInstance) {
            this.thisInstance = new WeakReference<InnerClassTest>(thisInstance);
        }

        public int callXPlusY() {
            InnerClassTest ict = thisInstance.get();
            if (ict != null) {
                return ict.x + ict.y;
            } else {
                throw new RuntimeException();
            }
        }
    }

    static interface StaticInterface {
        public int call();
    }

    public InnerClassTest(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void testStatic() {
        StaticInnerClass closureTypeThing = new StaticInnerClass(this);
        System.out.println(closureTypeThing.callXPlusY());
    }

    public void testAnonymous() {
        StaticInterface closureTypeThing = new StaticInterface() {
            public int call() {
                return InnerClassTest.this.x + InnerClassTest.this.y;
            }
        };
        System.out.println(closureTypeThing.call());
    }

    public static void main(String[] args) {
        InnerClassTest testClass = new InnerClassTest(42, 9000);
        testClass.testStatic();
        testClass.testAnonymous();
    }
}
