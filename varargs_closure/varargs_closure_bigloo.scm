(module simple_closure (main main))

(define add
  (lambda (x)
    (lambda (y)
      (+ x y))))

(define modify
  (lambda (n l)
    (if (null? l)
        '()
        (cons (+ (car l) n)
              (modify n (cdr l))))))

(define va
  (lambda (n)
    (lambda l
      (modify n l))))

(define add10 (add 10))

(define varargs-fun (va 2))

(define main
  (lambda (args)
    (display (add10 90))
    (display (varargs-fun 1 2 3))))
