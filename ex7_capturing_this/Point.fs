type Point (xv: int, yv: int) =
    member this.x: int = xv
    member this.y: int = yv
    override this.ToString () =
      sprintf "(%d, %d)" this.x this.y
    member this.CreatePrinter () =
      fun () -> printfn "%A" this
    member this.CreateAdder () =
      fun (pt: Point) -> Point(pt.x + this.x, pt.y + this.y)

let GetTestAdder () =
    let point = Point(10, 20)
    point.CreateAdder()

let GetTestPrinter () =
    let point = Point(42, 24)
    point.CreatePrinter()

[<EntryPoint>]
let Main argv =
    let adder = GetTestAdder ()
    let newPoint = adder(Point (9, 5))
    printfn "%A" newPoint

    let printer = GetTestPrinter ()
    printer()
    0
