import java.util.function.*;

class Point {
    public final int x;
    public final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Runnable getPrinter() {
        return () -> System.out.println(this);
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public Function<Point, Point> createAdder() {
        return point -> new Point(this.x + point.x, this.y + point.y);
    }

    public static Function<Point, Point> getTestAdder() {
        Point pt = new Point(10, 20);
        return pt.createAdder();
    }

    public static Runnable getTestPrinter() {
        Point pt = new Point(42, 24);
        return pt.getPrinter();
    }

    public static void main(String[] args) {
        Function<Point, Point> adder = getTestAdder();
        Point newPoint = adder.apply(new Point(9, 5));
        System.out.println(newPoint);

        Runnable printer = getTestPrinter();
        printer.run();
    }
}
