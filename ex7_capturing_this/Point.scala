class Point(xv: Int, yv: Int) {
  val x = xv
  val y = yv

  def createAdder(): Point => Point =
    point => new Point(this.x + point.x, this.y + point.y)

  def createPrinter(): () => Unit =
    () => println(this)

  override def toString(): String =
    "(" + x + ", " + y + ")"
}

object Test {
  def getTestAdder(): Point => Point = {
    val point = new Point(10, 20)
    point.createAdder()
  }

  def getTestPrinter(): () => Unit = {
    val point = new Point(42, 24)
    point.createPrinter()
  }

  def main(args: Array[String]): Unit = {
    val adder = getTestAdder()
    val newPoint = adder(new Point(9, 5))
    println(newPoint)

    val printer = getTestPrinter()
    printer()
  }
}
