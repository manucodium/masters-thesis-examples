using System;

public class Point
{
    public readonly int x;
    public readonly int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Func<Point, Point> CreateAdder() {
        return (point) => new Point(this.x + point.x, this.y + point.y);
    }

    public Action CreatePrinter() {
        return () => Console.WriteLine(this);
    }

    override public String ToString() {
        return "(" + x + ", " + y + ")";
    }

    public static Func<Point, Point> getTestAdder() {
        Point point = new Point(10, 20);
        return point.CreateAdder();
    }

    public static Action getTestPrinter() {
        Point point = new Point(42, 24);
        return point.CreatePrinter();
    }

    public static void Main() {
        var adder = getTestAdder();
        var newPoint = adder(new Point(9, 5));
        Console.WriteLine(newPoint);

        var printer = getTestPrinter();
        printer();
    }
}
