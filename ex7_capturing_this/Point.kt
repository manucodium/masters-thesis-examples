class Point constructor(val x: Int, val y: Int) {
    fun createPrinter() =
        { -> println(this) }

    fun createAdder() =
        { point: Point -> Point(this.x + point.x, this.y + point.y) }

    override fun toString(): String =
        "(${this.x}, ${this.y})"
}

fun getTestAdder(): (Point) -> Point {
    val pt = Point(10, 20)
    return pt.createAdder()
}

fun getTestPrinter(): () -> Unit {
    val pt = Point(42, 42)
    return pt.createPrinter()
}

fun main(args: Array<String>) {
    val adder = getTestAdder()
    val newPoint = adder(Point(9, 5))
    println(newPoint)

    val printer = getTestPrinter()
    printer()
}
