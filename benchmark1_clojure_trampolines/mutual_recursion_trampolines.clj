; Trampoliineilla
(ns mutual-recursion-trampolines
  (:gen-class))

(declare is-even-fn)

; Palauttaa nyt sulkeuman
(defn is-odd-fn [n]
  (do
    (if (zero? n)
      false
      #(is-even-fn (- n 1)))))

(defn is-even-fn [n]
  (do
    (if (zero? n)
      true
      #(is-odd-fn (- n 1)))))

(defn -main []
  (doseq [i (repeat 50 1000)]
    (println (time (trampoline is-odd-fn i)))
    (println (time (trampoline is-even-fn i)))))
