; Ilman häntäkutsuoptimointia
(ns mutual-recursion
  (:gen-class))

(declare is-even)

(defn is-odd [n]
  (do
    (if (zero? n)
      false
      (is-even (- n 1)))))

(defn is-even [n]
  (do
    (if (zero? n)
      true
      (is-odd (- n 1)))))

(defn -main []
  (doseq [i (repeat 50 1000)]
    (println (time (is-odd i)))
    (println (time (is-even i)))))
