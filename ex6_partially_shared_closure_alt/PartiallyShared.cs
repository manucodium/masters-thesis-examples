using System;

public class Counter
{
    public static Func<int> getX;
    public static Func<int> getY;
    public static Action<int> setX;
    public static Action<int> setY;
    public static Func<int> getOffsetX;
    public static Func<int> getOffsetY;

    public static void create(int xval, int yval, int offsetval) {
        int x = xval;
        int y = yval;
        int offset = offsetval;

        getX = () => x;
        getY = () => y;
        setX = (val) => x = val;
        setY = (val) => y = val;
        getOffsetX = () => x + offset;
        getOffsetY = () => y + offset;
    }

    public static void Main() {
        create(10, 5, 1000);
        Console.WriteLine(getX());
        Console.WriteLine(getY());
        setX(42);
        setY(62);
        Console.WriteLine(getOffsetX());
        Console.WriteLine(getOffsetY());
    }
}
