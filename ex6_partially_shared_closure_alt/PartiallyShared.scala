object PartiallyShared {
  var getX: () => Int = null
  var getY: () => Int = null
  var setX: Int => Unit = null
  var setY: Int => Unit = null
  var getOffsetX: () => Int = null
  var getOffsetY: () => Int = null

  def main(args: Array[String]): Unit = {
    create(10, 5, 1000);
    println(getX())
    println(getY())
    setX(42)
    setY(62)
    println(getOffsetX())
    println(getOffsetY())
  }

  def create(xval: Int, yval: Int, offsetval: Int): Unit = {
    var x: Int = xval
    var y: Int = yval
    var offset: Int = offsetval

    getX = () => x
    getY = () => y
    setX = value => x = value
    setY = value => y = value
    getOffsetX = () => x + offset
    getOffsetY = () => y + offset
  }
}
