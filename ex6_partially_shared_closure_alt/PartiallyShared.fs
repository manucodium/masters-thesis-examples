let createPoint xval yval offsetval =
    let x = ref xval // ref-tyyppi
    let y = ref yval
    let offset = ref offsetval

    let setX = fun v -> x := v
    let setY = fun v -> y := v
    let getX = fun () -> !x
    let getY = fun () -> !y
    let getXWithOffset = fun () -> !x + !offset
    let getYWithOffset = fun () -> !y + !offset
    (setX, setY, getX, getY, getXWithOffset, getYWithOffset)

[<EntryPoint>]
let main argv =
    let (setX, setY, getX, getY, getXWithOffset, getYWithOffset) = createPoint 10 5 1000
    printfn "%d %d" (getX ()) (getY ())
    setX 42
    setY 62
    printfn "%d %d" (getXWithOffset ()) (getYWithOffset ())
    0

