var getX: (() -> Int)? = null
var getY: (() -> Int)? = null
var setX: ((Int) -> Unit)? = null
var setY: ((Int) -> Unit)? = null
var getOffsetX: (() -> Int)? = null
var getOffsetY: (() -> Int)? = null

fun createPair(xval: Int, yval: Int, offsetval: Int): Unit {
    var x = xval
    var y = yval
    var offset = offsetval

    getX = { -> x }
    getY = { -> y }
    getOffsetX = { -> x + offset }
    getOffsetY = { -> y + offset }
    setX = { value -> x = value }
    setY = { value -> y = value }
}

fun main(args: Array<String>) {
    createPair(10, 5, 1000)
    println(getX!!())
    println(getY!!())
    setX!!(42)
    setY!!(62)
    println(getOffsetX!!())
    println(getOffsetY!!())
}

