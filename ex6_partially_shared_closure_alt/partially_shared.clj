(ns partially-shared
  (:gen-class))

(defn create-point [x-val y-val offset-val]
  (let [x (atom x-val)
        y (atom y-val)
        offset (atom offset-val)]
    { :get-x (fn [] @x)
      :get-y (fn [] @y)
      :set-x (fn [value] (reset! x value))
      :set-y (fn [value] (reset! y value))
      :get-offset-x (fn [] (+ @x @offset))
      :get-offset-y (fn [] (+ @y @offset)) }))

(defn -main []
  (let [point (create-point 10 5 1000)]
    (println ((:get-x point)))
    (println ((:get-y point)))
    ((:set-x point) 42)
    ((:set-y point) 62)
    (println ((:get-offset-x point)))
    (println ((:get-offset-y point)))))

