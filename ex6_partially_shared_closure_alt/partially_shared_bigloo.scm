(module partially-shared (main main))

(define get-x #f)
(define set-x #f)
(define get-y #f)
(define set-y #f)

(define create-pair
  (lambda ()
    (define x 0)
    (define y 0)

    (set! get-x
      (lambda () x))
    (set! get-y
      (lambda () y))

    (set! set-x
      (lambda (val)
        (set! x val)))
    (set! set-y
      (lambda (val)
        (set! y val)))))

(define main
  (lambda (args)
    (create-pair)
    (set-x 42)
    (set-y -15)
    (print (get-x))
    (print (get-y))))

