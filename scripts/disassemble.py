import sys
import os
from group import Group

class DisassemblerError(Exception):
    pass

def list_files(path, platform = "jvm"):
    extension = None
    if platform == "jvm":
        extension = "class"
    elif platform == "mono":
        extension = "exe"
    else:
        raise DisassemblerError("Unknown platform '" + platform + "'");

    return [path + "/" + x for x in os.listdir(path) if x.endswith("." + extension)]

def get_disassembler(platform):
    def disassemble_jvm(filePath, targetPath):
        os.system("javap -c -s -l -verbose -private '" + filePath + "' > '" + targetPath + "'")

    def disassemble_mono(filePath, targetPath):
        os.system("monodis '" + filePath + "' > '" + targetPath + "'")

    if platform == "jvm":
        return disassemble_jvm
    elif platform == "mono":
        return disassemble_mono
    else:
        return None

def ensure_output_directory(dirName):
    if os.path.exists(dirName):
        [os.remove(dirName + "/" + f) for f in os.listdir(dirName) if f.endswith(".dasm")]
    else:
        os.makedirs(dirName)

def disassemble_all(buildDir, targetDir, platform):
    files = list_files(buildDir, platform)
    disassembler = get_disassembler(platform)
    if (len(files) == 0):
        raise DisassemblerError("No files to disassemble")
    if (not disassembler):
        raise DisassemblerError("No disassembler found for platform '" + platform + "'")

    ensure_output_directory(targetDir)

    for filePath in files:
        fileName = filePath.split("/")[-1]
        platformPart = ""
        if platform == "mono":
            platformPart = ".mono"
        targetPath = targetDir + "/" + fileName + platformPart + ".dasm"

        print "Disassembling " + filePath
        print "> " + targetPath
        disassembler(filePath, targetPath)

# TODO: better command line argument handling
def main():
    buildDir = None
    targetDir = None
    platform = "jvm"
    args = sys.argv[1:]

    if args[0] == "--help":
        print ("Usage: disassemble [--help] [-i buildDir] [-o outputDir] [-p platform]\n\n"
               "--help     print out this help message\n"
               "-i         input (build) directory\n"
               "-o         output directory\n"
               "-p         platform (jvm or mono)\n")
        return

    for flag, param in Group(sys.argv[1:], 2):
        if flag == "-i":
            buildDir = param
        elif flag == "-o":
            targetDir = param
        elif flag == "-p":
            platform = param

    if not buildDir or not targetDir:
        print "No input or output directories specified"
        sys.exit(1)

    try:
        disassemble_all(buildDir, targetDir, platform)
    except DisassemblerError, e:
        print "Error: ", e
        return

if __name__ == "__main__":
    main()
