class Group:
    def __init__(self, l, size):
        self.size = size
        self.args = l

    def __getitem__(self, i):
        index = i * self.size
        if index > len(self.args):
            raise IndexError("Index out of range")

        values = self.args[index:index+self.size]
        return values[0], values[1]
