from contextlib import contextmanager
import os
import sys
import shutil
import re
from group import Group

mono_libs = "/Library/Frameworks/Mono.framework//Versions/4.2.1/lib/mono/4.5/System.Numerics.dll"

tempDirName = "temp"

class CompileError(Exception):
    pass

@contextmanager
def pushd(newDir):
    previousDir = os.getcwd()
    os.chdir(newDir)
    yield
    os.chdir(previousDir)

def create_tempdir(tempDirName):
    if os.path.exists(tempDirName):
        raise CompileError("Temporary directory '" + tempDirName + "' already exists")
    else:
        os.makedirs(tempDirName)

def setup(dirName = None):
    global tempDirName

    if dirName:
        tempDirName = dirName
    else:
        scriptPath = os.path.realpath(__file__).rsplit("/", 1)[0]
        tempDirConfig = scriptPath + "/BUILD_DIR_NAME"
        if os.path.exists(tempDirConfig):
            with open(tempDirConfig) as f:
                tempDirName = f.readlines()[0].strip()

    create_tempdir(tempDirName)

def cleanup():
    if os.path.exists(tempDirName):
        shutil.rmtree(tempDirName)

@contextmanager
def tempdir_for(target):
    shutil.copyfile(target, tempDirName + "/" + target)

    with pushd(tempDirName):
        yield
        os.remove(target)


def get_platform(target):
    extension = target.rsplit(".", 1)[-1]

    if extension == "cs" or extension == "fs":
        return "mono"
    else:
        return "jvm"

def compile(target, compiler = None, compiler_flags = None):
    parts = target.split(".")
    name = parts[0]
    extension = parts[-1]

    cmd = ""

    if extension == "scm":
        if not compiler:
            raise CompileError("No Scheme compiler specified, exiting")

        if compiler == "bigloo":
            cmd = "bigloo -jvm " + compiler_flags + " '" + target + "'"
        elif compiler == "kawa":
            kawa = "java -cp '/Users/tuuli/school/scheme_implementations/*' kawa.repl"
            cmd = kawa + " --main " + compiler_flags + " -C '" + target + "'"
        else:
            raise CompileError("Unknown Scheme compiler '" + compiler + "'")
    elif extension == "clj":
        namespace = re.sub("_", "-", name)
        clojure = "java -cp ~/clojure-1.8.0/clojure-1.8.0.jar:. clojure.main"
        cmd = clojure + " -e \"(set! *compile-path* \\\".\\\")(compile '" + namespace + ")\""
    elif extension == "scala":
        cmd = "scalac " + compiler_flags + "'" + target + "'"
    elif extension == "java":
        cmd = "javac " + compiler_flags + "'" + target + "'"
    elif extension == "kt":
        cmd = "kotlinc " + compiler_flags + "'" + target + "'"
    elif extension == "cs":
        cmd = "mcs " + compiler_flags + " -r:" + mono_libs + " '" + target + "'"
    elif extension == "fs":
        cmd = "fsharpc --optimize- " +  compiler_flags + "'" + target + "'"
    else:
        raise CompileError("Unknown file extension '" + extension + "'")

    print cmd
    os.system(cmd)

def main():
    if len(sys.argv) < 2:
        print "No target specified"
        print ("Usage: python compile.py target [-c compiler] [-C compile-flags]\n"
               "Valid values for compiler are: kawa, bigloo.\n"
               "Compile flags are passed directly to the compiler")
        sys.exit(1)

    target = sys.argv[1]
    compiler = None
    compiler_flags = ""

    for flag, param in Group(sys.argv[2:], 2):
        if flag == "-c":
            compiler = param
        elif flag == "-C":
            compiler_flags = param
        else:
            print "Unknown flag '" + flag + "'"
            sys.exit(1)

    setup("build")
    error = False
    with tempdir_for(target):
        try:
            compile(target, compiler, compiler_flags)
        except CompileError, ce:
            print "Error: ", ce
            error = True
        except:
            error = True

    if error:
        cleanup()
        sys.exit(1)

if __name__ == "__main__":
    main()
