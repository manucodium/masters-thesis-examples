import sys
import disassemble
import compile
from group import Group

usage_string = "Usage: python compile_and_disassemble.py target output_dir [-c compiler]"

def main():
    if len(sys.argv) < 3:
        print usage_string
        return

    target = sys.argv[1]
    output_dir = sys.argv[2]
    platform = compile.get_platform(target)
    compiler = None
    compiler_flags = ""

    for flag, param in Group(sys.argv[3:], 2):
        if flag == "-c":
            compiler = param
        elif flag == "-C":
            compiler_flags = param
        else:
            print "Unknown flag " + flag
            print usage_string
            return

    try:
        compile.setup()
        with compile.tempdir_for(target):
            compile.compile(target, compiler, compiler_flags)

        disassemble.disassemble_all(compile.tempDirName, output_dir, platform)
    except compile.CompileError, ce:
        print "Error: ", ce
    except disassemble.DisassemblerError, de:
        print "Error: ", de
    finally:
        compile.cleanup()

if __name__ == "__main__":
    main()
