#lang r5rs

(define sum
  (lambda vals
    (define sum-helper
      (lambda (l acc)
        (if (null? l)
            acc
            (sum-helper (cdr l)
                        (+ acc (car l))))))
    (sum-helper vals 0)))

