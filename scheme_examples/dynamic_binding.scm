; Kääntyy Kawalla
(define n 1)

(define add-n
  (lambda (x)
    (+ n x)))

(define add-static
  (lambda (x)
    ; let-rakenne muodostaa leksikaalisen näkyvyysalueen,
    ; joten add-n ei näe uutta n-muuttujan arvoa.
    (let ((n 2))
      (add-n x))))

(define add-dynamic
  (lambda (x)
    ; fluid-let-rakenne sitoo n-muuttujan arvon dynaamisesti
    ; uudelleen muodostamansa lohkon sisällä, jolloin add-n
    ; näkee n-muuttujan uuden arvon
    (fluid-let ((n 2))
      (add-n x))))

(display (add-dynamic 1)) ; tulostaa 3
(display (add-static 1))  ; tulostaa 2

