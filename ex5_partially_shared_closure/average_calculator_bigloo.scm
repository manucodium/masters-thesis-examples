(module average-calculator (main main))

(define add-value #f)
(define get-average #f)

(define init-averager
  (lambda ()
    (define vals '())
    (define val-count 0)

    ; captures vals
    (define sum
      (lambda ()
        (define f
          (lambda (l acc)
            (if (null? l)
                acc
                (f (cdr l)
                   (+ acc (car l))))))
        (f vals 0)))

    ; captures val and val-count
    (set! add-value
      (lambda (x)
        (set! vals (cons x vals))
        (set! val-count (+ val-count 1))))

    ; captures sum and val-count
    (set! get-average
      (lambda ()
        (/ (sum) val-count)))))

(define main
  (lambda (args)
    (init-averager)
    (add-value 10)
    (add-value 42)
    (add-value 19)
    (add-value 25)
    (print (get-average))))

