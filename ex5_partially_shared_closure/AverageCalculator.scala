object AverageCalculator {
  var addValue: Int => Unit = null
  var getAverage: () => Int = null

  def main(args: Array[String]): Unit = {
    initCalculator()
    addValue(10)
    addValue(42)
    addValue(19)
    addValue(25)
    println(getAverage())
  }

  def initCalculator(): Unit = {
    var vals: List[Int] = List()
    var valCount: Int = 0

    def sum() = vals.reduceLeft(_ + _)

    addValue = value => {
      vals = value :: vals
      valCount = valCount + 1
    }

    getAverage = () => { sum() / valCount }
  }
}
